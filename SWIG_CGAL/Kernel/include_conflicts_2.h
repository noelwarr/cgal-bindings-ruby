// ------------------------------------------------------------------------------
// Copyright (c) 2011 GeometryFactory (FRANCE)
// Distributed under the Boost Software License, Version 1.0. (See accompany-
// ing file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
// ------------------------------------------------------------------------------ 


#ifndef SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_2_H
#define SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_2_H

class Line_2;
class Ray_2;
class Direction_2;
class Vector_2;
class Segment_2;

#endif //SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_2_H
