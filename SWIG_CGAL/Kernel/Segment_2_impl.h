// ------------------------------------------------------------------------------
// Copyright (c) 2011 GeometryFactory (FRANCE)
// Distributed under the Boost Software License, Version 1.0. (See accompany-
// ing file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
// ------------------------------------------------------------------------------ 


#ifndef SWIG_CGAL_KERNEL_SEGMENT_2_IMPL_H
#define SWIG_CGAL_KERNEL_SEGMENT_2_IMPL_H
//member functions involving elements that are in include conflict
SWIG_CGAL_FORWARD_CALL_AND_REF_SCOPE_0(Direction_2,Segment_2::direction,direction)
SWIG_CGAL_FORWARD_CALL_AND_REF_SCOPE_0(Vector_2,Segment_2::to_vector,to_vector)
SWIG_CGAL_FORWARD_CALL_AND_REF_SCOPE_0(Line_2,Segment_2::supporting_line,supporting_line)
#endif //SWIG_CGAL_KERNEL_SEGMENT_2_IMPL_H
