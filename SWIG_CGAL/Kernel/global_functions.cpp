// ------------------------------------------------------------------------------
// Copyright (c) 2011 GeometryFactory (FRANCE)
// Distributed under the Boost Software License, Version 1.0. (See accompany-
// ing file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
// ------------------------------------------------------------------------------ 


#include <SWIG_CGAL/Kernel/global_functions.h>

#define NO_SWIG_CGAL_GF_MACRO_DEFINITION

#include <SWIG_CGAL/Common/global_function_macros.h>

#include <SWIG_CGAL/Kernel/global_function_signatures.h>
