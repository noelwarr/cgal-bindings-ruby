// ------------------------------------------------------------------------------
// Copyright (c) 2011 GeometryFactory (FRANCE)
// Distributed under the Boost Software License, Version 1.0. (See accompany-
// ing file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
// ------------------------------------------------------------------------------ 


#ifndef SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_3_H
#define SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_3_H

class Ray_3;
class Direction_3;
class Vector_3;
class Line_3;
class Segment_3;
class Plane_3;

#endif //SWIG_CGAL_KERNEL_INCLUDE_CONFLICTS_3_H
